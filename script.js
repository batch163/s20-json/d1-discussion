

/*
	JavaScript JSON

		- The JavaScript JSON is an acronym of JavaScript Object Notation. 
		- It provides a format for storing and transporting data. 
		- It is a lightweight human readable collection of data that can be accessed in a logical manner.

	Points to remember:
		- It generates and stores the data from user input.
		- It can transport the data from the server to client, client to server, and server to server.
		- It can also build and verifying the data.


	JSON Syntax:

		1. While working with .json file, the syntax will be like:

				{  
		            "First_Name": "value";  
		            "Last_Name": "value ";  
		        }  

		2. While working with JSON object in .js or .html file, the syntax will be like:

		let varName ={  
	        "First_Name": "value";  
	        "Last_Name": "value ";  
	    }  


	JavaScript JSON Methods:

		- Let's see the list of JavaScript JSON method with their description.

				
	JSON.parse()
		- This method takes a JSON string and transforms it into a JavaScript object.

	JSON.stringify()
		- This method converts a JavaScript value (JSON object) to a JSON string representation.


	JavaScript JSON Example
		- Let's see an example to convert string in JSON format using parse() and stringify() method.

	
Example:
*/

	let batchArr = [
		{ 
			name: "Angelito",
			batch: "B163"
		},
		{
			name: "Ian",
			batch: "B163"
		}
	];

	console.log(batchArr)

	batchArr.forEach(data => console.log(data))

	//to convert from js objects & arrays to json, we will use stringify()
	const convertedToStr = JSON.stringify(batchArr);
	console.log(convertedToStr)

	// converted.forEach(data => console.log(data))
		//not possible to use JS methods if the state is still in json/sting


	// console.log(JSON.parse(converted))
	const convertedToJS = JSON.parse(convertedToStr)

	convertedToJS.forEach(data => console.log(data))
		//possible to use JS methods if the state is on JS object/array



